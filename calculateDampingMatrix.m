function D = calculateDampingMatrix(nu, m_Dlf, m_Dqf)
            
    [velRow, velCols] = size(nu);

    if(velRow == 1 || velCols ~= 1)
        
        ME = MException('MyComponent:incompatiblePosition', ...
                'Position vector does not match a column vector!');

        throw(ME);
        
    else

        tmpMtrx = abs([nu nu nu nu nu nu]);

        D = m_Dlf + m_Dqf.*tmpMtrx;

    end

end