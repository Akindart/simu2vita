%==================== Opening virtual world ===============================

vita_world = vrworld('vita_world.x3dv');
open(vita_world);


nh_pf = vrnode(vita_world, 'ping_forward_sensor')
sync(nh_pf, 'pickedRange', 'on')
nh_pr = vrnode(vita_world, 'ping_right_sensor')
sync(nh_pr, 'pickedRange', 'on')
nh_pl = vrnode(vita_world, 'ping_left_sensor')
sync(nh_pl, 'pickedRange', 'on')
nh_pd = vrnode(vita_world, 'ping_down_sensor')
sync(nh_pd, 'pickedRange', 'on')
nh_pu = vrnode(vita_world, 'ping_up_sensor')
sync(nh_pu, 'pickedRange', 'on')

%==================== Initial state of the vehicle ========================

% init_vehicle_position = [3 1 -1]' %[x y z]' in [m]

init_vehicle_position = [0 0 0]' %[x y z]' in [m]


%init_vehicle_orientation = [30 -175 15]' %[roll pitch yaw]' in [degree]

%init_vehicle_orientation = [.705   -0.0277    0.0319    0.709]';

%[x, y, z] = quat2angle(init_vehicle_orientation', 'XYZ');

%init_vehicle_orientation_ang = rad2deg([x y z])'; %[roll pitch yaw]' in [degree]

% init_vehicle_orientation_ang = [-5 20 15]'; %[roll pitch yaw]' in [degree]
init_vehicle_orientation_ang = [0 0 0]'; %[roll pitch yaw]' in [degree]

init_vehicle_orientation = eul2quat(deg2rad(init_vehicle_orientation_ang'), 'XYZ')'

vita_world.body.translation = init_vehicle_position';
vita_world.body.rotation = quat2axang(init_vehicle_orientation');

vw_view = view(vita_world)
close(vw_view)


nh_pl.pickedRange
nh_pr.pickedRange

init_sway_offset = nh_pl.pickedRange - nh_pr.pickedRange

nh_pu.pickedRange
nh_pd.pickedRange

init_heave_offset = nh_pu.pickedRange - nh_pd.pickedRange



init_vehicle_linear_vel = [0 0 0]' %[u v w]' in [m/s]

init_vehicle_angular_vel = [0 0 0]' %[p q r]' in [n.d.]

init_vehicle_angular_vel_ned = [0 0 0]' %[r p y]' in [rad/s]

const_water_current_vel = [0 0 0]' %[c_u c_v c_w]' in [m/s]

%==================== Reference values of the guidance system =============

surge_vel_ref = 0.2 %0.1 0.2 0.25 0.3 0.4 %[m/s]

sway_offset_ref = 0 %[m]

heave_offset_ref = 0 %[m]

roll_ref = 0 %[degree]

pitch_ref = 0 %[degree]

yaw_ref = 0 %[degree]

%==================== Echoshourders config ================================

tunel_pose_wrt_NED = [0 0 0 1 0 0 0]' %[x y z | q0 q1 q2 q3] in [m | quaternion]

tunnel_height = 16 %[m]

tunnel_width = 16 %[m]

echosounders_error_rate = 0.5 % in percetagess

%==================== Thrusters configuration =============================


thruster_time_constant = 0.1754; 

%==================== Non-linear sampling frequency =======================

nlinear_sf = 400; %[Hz]

%==================== First loop detection variables ======================

first_loop = 1;
first_pos = [0 0 0]';

%==================== Closing virtual world ===============================


close(vita_world)
