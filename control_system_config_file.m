%============== Surge Vel PI Path follower parameters =====================

sur_vel_gamma = 0.2;

% Kp_sur_vel = sur_vel_gamma*2;
% Ki_sur_vel = sur_vel_gamma*sur_vel_gamma;

Kp_sur_vel = 1;
Ki_sur_vel = 0.001;

surge_controller_freq = 20;

%============== Sway offset PID Path follower parameters ==================

sway_off_gamma = 0.2;

% Kp_sway_off = sway_off_gamma*sway_off_gamma*3;
% Ki_sway_off = sway_off_gamma*sway_off_gamma*sway_off_gamma;
% Kd_sway_off = sway_off_gamma*3;

Kp_sway_off = 0.25;
Ki_sway_off = 0.025;
Kd_sway_off = 0.5;

sway_controller_freq = 20;

%============== Heave offset PID Path follower parameters =================

heave_off_gamma = 0.2;

% Kp_heave_off = heave_off_gamma*heave_off_gamma*3;
% Ki_heave_off = heave_off_gamma*heave_off_gamma*heave_off_gamma;
% Kd_heave_off = heave_off_gamma*3;

Kp_heave_off = 0.25;
Ki_heave_off = 0.025;
Kd_heave_off = 0.5;

% Kp_heave_off = 10;
% Ki_heave_off = 2.5;
% Kd_heave_off = 5;

heave_controller_freq = 20;

%============== Orientation Path follower parameters ======================
% PD² approach

Kp_quat = 5;

Kd_ang_vel_msrd = 3;

Kd_ang_vel_dsrd = 2;

% Kp_quat = 0.25;
% 
% Kd_ang_vel_msrd = 0.15;
% 
% Kd_ang_vel_dsrd = 0.1;

% Kp_quat = 0.00000001;
% 
% Kd_ang_vel_msrd = 0.00000000001;
% 
% Kd_ang_vel_dsrd = 0.00000000001/3;