function [expanded_array] = expand_array_by_groups(group_array, value_by_group)

    num_elements = sum(group_array);

    expanded_array = ones(num_elements, 1);
    
    are_groups_equal_in_size = isequal(size(group_array, [1 2]), size(value_by_group, [1 2]));
       
    assert(are_groups_equal_in_size, ...
        'Qty of groups and qty of value by group do not match.');
    
    index_end = 0;
    
    for i = 1:size(group_array, 1)

             index_begining = 1;

             if i ~= 1
                 index_begining = index_end + 1;
             end

             index_end = index_begining + group_array(i) - 1;

             expanded_array(index_begining:index_end) = ... 
                 expanded_array(index_begining:index_end)*value_by_group(i);

    end

end