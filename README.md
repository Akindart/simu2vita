# Simu2VITA

A simple simulator built on MATLAB and Simulink framework and inspired by other Matlab blocks.
We aimed for simple and rapid prototipation with good physics simulation. Simu2VITA simulate 
actuators and the dynamics of the rigid body of the vehicle.

# Citation

If this simulator was useful for you, please consider cite it in your work :).

```
@Article{dCG2022,
    AUTHOR = {de Cerqueira Gava, Pedro Daniel and Nascimento Júnior, Cairo Lúcio and Belchior de França Silva, Juan Ramón and Adabo, Geraldo José},
    TITLE = {Simu2VITA: A General Purpose Underwater Vehicle Simulator},
    JOURNAL = {Sensors},
    VOLUME = {22},
    YEAR = {2022},
    NUMBER = {9},
    ARTICLE-NUMBER = {3255},
    URL = {https://www.mdpi.com/1424-8220/22/9/3255},
    PubMedID = {35590945},
    ISSN = {1424-8220},
    DOI = {10.3390/s22093255}
}

```